//----- hamburger -----//

const burgerBtn = document.querySelector('.header__burger'),
      navbar    = document.querySelector('.header__navbar'),
      navPopup  = document.querySelector('.header__popup');
// hamburger
burgerBtn.addEventListener('click', () => { 
    burgerBtn.classList.toggle('burger__active');
    navbar.classList.toggle('navbar__active');
    navPopup.classList.toggle('popup__active');
});
// popup
navPopup.addEventListener('click', () => {
    burgerBtn.classList.remove('burger__active');
    navbar.classList.remove('navbar__active');
    navPopup.classList.remove('popup__active');
});
//----- slider season -----//

const seasonTrack = document.querySelector('.season__track'),
      seasonPrev  = document.querySelector('.season__prev'),
      seasonNext  = document.querySelector('.season__next'),
      seasonDots  = document.querySelectorAll('.season__dots div');

let seasonPosition = 0,
    seasonNum      = 0;
// button next
seasonNext.addEventListener('click', () => {
    // season next img
    seasonPosition == 310 * 2 ? seasonPosition = 0 : seasonPosition += 310;
    seasonTrack.style.transform = `translate(-${seasonPosition}px)`;
    // season next dots
    seasonDots[seasonNum].classList.remove('dots__active');
    seasonNum + 1 == 3 ? seasonNum = 0 : seasonNum++;
    seasonDots[seasonNum].classList.add('dots__active');
});
// button prev
seasonPrev.addEventListener('click', () => {
    // season prev img
    seasonPosition == 0 ? seasonPosition = 310 * 2 : seasonPosition -= 310;
    seasonTrack.style.transform = `translate(-${seasonPosition}px)`;
    // season prev dots
    seasonDots[seasonNum].classList.remove('dots__active');
    seasonNum - 1 == -1 ? seasonNum = seasonDots.length - 1 : seasonNum--;
    seasonDots[seasonNum].classList.add('dots__active');
});
//dots function
function dotsClick(){
    for(item of seasonDots){
        item.classList.remove('dots__active');
    }
    seasonDots[seasonNum].classList.add('dots__active');
    seasonTrack.style.transform = `translate(-${seasonPosition}px)`;
}
//dots season
seasonDots.forEach(item => {
    item.addEventListener('click', () => {
        if(item.id == 'dot-one'){
            seasonPosition = 0,
            seasonNum      = 0;
            dotsClick()
        }
        else if(item.id == 'dot-two'){
            seasonPosition = 310,
            seasonNum      = 1;
            dotsClick()
        }
        else if(item.id == 'dot-three'){
            seasonPosition = 310 * 2,
            seasonNum      = 2;
            dotsClick()
        }   
    });
});

//----- slider prices -----//
const pricesTrack = document.querySelector('.prices__track'),
      pricesTop = document.querySelector('.prices__top'),
      pricesBottom = document.querySelector('.prices__bottom');

let pricesPosition = 0;
// button top
pricesTop.addEventListener('click', () => {
    pricesPosition == 470 * 7 ? pricesPosition = 0 : pricesPosition += 470;
    pricesTrack.style.transform = `translateY(-${pricesPosition}px)`;
});
// button bottom
pricesBottom.addEventListener('click', () => {
    pricesPosition == 0 ? pricesPosition = 470 * 7 : pricesPosition -= 470;
    pricesTrack.style.transform = `translateY(-${pricesPosition}px)`;    
});

//----- slider brands -----//
const brandsTrack = document.querySelector('.brands__track'),
      brandsBtn = document.querySelector('.brands__btn button');

let brandsPosition = 0;

brandsBtn.addEventListener('click', () => {
    brandsPosition == 100 * 3 ? brandsPosition = 0 : brandsPosition += 100;
    brandsTrack.style.transform = `translateY(-${brandsPosition}px)`;
});

//----- valid form -----//
const inputEmail  = document.querySelector('.mailing__form input'),
      formEmail   = document.querySelector('.mailing__form'),
      btnEmail    = document.querySelector('.mailing__form button'),
      validResult = document.querySelector('.valid__result'), 
      pattern     = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;

inputEmail.addEventListener('change', () => {
    if(inputEmail.value.match(pattern)){
        validResult.innerText = 'Your Email Address in Valid';
        validResult.style.color = 'green';
        validResult.style.display = 'block';
        setTimeout(() => {
            validResult.style.display = 'none';
        }, 3000);
    }
    else if (inputEmail.value == ''){
        validResult.innerText = 'Please Enter Valid Email Address';
        validResult.style.color = 'red';
    }
    else {
        validResult.innerText = 'Please Enter Valid Email Address';
        validResult.style.color = 'red';
    };
});

formEmail.onsubmit = e => e.preventDefault();

//----- button top -----//
const btnTop = document.querySelector('.btn__top');

//  button top show
window.addEventListener('scroll', () => {
    window.pageYOffset > 1300 ? btnTop.classList.add('show') : btnTop.classList.remove('show');
});

// button click top
btnTop.addEventListener('click', () => window.scroll(0, 0));
